import './static/css/w3.css'
import './static/css/fonts.css'
import './static/css/style.css'
import './static/css/c-colors.css'

import Config from './config.js'
import Provider from './provider/provider.js'

import Auth from './controllers/pages/auth/auth.js'
import Router from './controllers/router/router.js'

import Alerter from './components/alerter/alerter.js'
import GlobalListeners from './controllers/tools/globalListeners/globalListeners.js'
import Pager from './controllers/tools/pager/pager.js'
import serializer from './components/serializer/serializer.js'
import Cache from './controllers/tools/cache/cache.js'

import Navigator from './controllers/pages/navigator/navigator.js'
import Menu from './controllers/pages/menu/menu.js'

import ItsAFilter from '@vlsp/its-a-filter'

customElements.define('its-a-filter', ItsAFilter)

// Setup context
const ctx = {
  db: new Provider(Config.api),
  target: document.querySelector('div#sitecontent'),
  tableTarget: document.querySelector('div#tablecontent'),
  alerter: new Alerter(),
  serializer,
  fontSize: 12,
  pdfFontSize: 10,
}

// Setup cache
ctx.cache = new Cache(ctx)

// Setup auth
const auth = new Auth(ctx)
ctx.auth = auth

// Setup menu
const menu = new Menu('div#globalmenu', ctx)
ctx.menu = menu

// Setup router
const router = new Router(
  {
    default: new Navigator(ctx),
    // "#about": new About(ctx)
  },
  ctx
)
ctx.router = router

// Execution
ctx.auth.run().then(() => {
  // Setup global listeners
  const gl = new GlobalListeners(ctx)
  gl.set()
  // on authentication success
  // start router
  ctx.router.start()
  // setup pager (need userid to store its state)
  ctx.pager = new Pager(ctx)
})
