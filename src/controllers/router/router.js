export default class Router {
  constructor(routes, ctx) {
    this.routes = routes
    this.ctx = ctx
    this.extraRoutes = {}

    if (!this.routes.hasOwnProperty('default')) {
      console.error('ROUTER ERROR: Default route is not set')
    }
  }

  start() {
    window.addEventListener('hashchange', this)
    this.routeIfAuthenticated()
  }

  handleEvent() {
    this.routeIfAuthenticated()
  }

  routeIfAuthenticated() {
    this.cleanUp()
    this.ctx.auth.run().then(() => this.select().run())
  }

  select() {
    let route = this.routes['default']
    const hash = this.getHash()

    if (this.routes.hasOwnProperty(hash)) {
      route = this.routes[hash]
    } else if (this.extraRoutes.hasOwnProperty(hash)) {
      route = this.extraRoutes[hash]
    }

    return route
  }

  getHash() {
    let hash = window.location.hash

    // Ignore reload_{something} hash suffix. For page reloading.
    const idx = hash.indexOf('_reload_')
    if (idx !== -1) {
      hash = hash.slice(0, idx)
    }

    return hash
  }

  extra(routes) {
    this.extraRoutes = routes
  }

  cleanUp() {
    this.ctx.target.innerHTML = ''
    this.ctx.tableTarget.innerHTML = ''
  }

  reload() {
    // remember scroll
    const scrollWrapper = document.querySelector('div#scrollwrapper')
    if (scrollWrapper) {
      const left = scrollWrapper.scrollLeft
      const top = document.documentElement.scrollTop
      this.ctx.scroll = { top, left }
    }
    // clear data outdate indicator
    if (
      this.ctx.hasOwnProperty('outdateIndicator') &&
      this.ctx.outdateIndicator
    ) {
      clearTimeout(this.ctx.outdateIndicator)
    }
    // reload
    document.location.hash = this.getHash() + `_reload_${Date.now()}`
  }
}
