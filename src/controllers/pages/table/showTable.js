import Composer from '../../tools/composer/composer.js'
import ScheetProcessor from '../../tools/scheetProcessor/scheetProcessor.js'

import showTableTmpl from './views/showTable.hbs'

const composer = new Composer()

export default class ShowTable {
  constructor(schema, ctx) {
    this.schema = schema
    this.ctx = ctx
    this.processor = new ScheetProcessor(ctx)
  }

  run() {
    this.ctx.schema = this.schema
    this.ctx.pager.init()

    this.makeQuery()
      .then((res) => Promise.all(res.map((r) => r.json())))
      .then((res) => {
        this.showTable(res)
        if (this.ctx.hasOwnProperty('scroll') && this.ctx.scroll) {
          window.scrollTo({ top: this.ctx.scroll.top })
          document
            .querySelector('div#scrollwrapper')
            .scrollTo({ left: this.ctx.scroll.left })
        }
        this.processor.run()
      })
  }

  makeQuery() {
    return this.ctx.db.get(
      this.schema.query,
      this.ctx.pager.getStatus() // set row count and offset
    )
  }

  showTable([size, data]) {
    // tune pager
    this.ctx.pager.setCount(size._size)
    // prepare data
    const rows = composer.join(this.schema.cols, data)
    // clear site area
    this.ctx.target.innerHTML = ''
    // show table and its controls
    this.ctx.tableTarget.innerHTML = showTableTmpl({
      schema: this.schema,
      rows,
      pager: this.ctx.pager.getStatus(),
    })
    // set font size
    const table = document.querySelector('table.m-table')
    if (table && this.ctx.fontSize) {
      table.style.fontSize = this.ctx.fontSize + 'px'
    }
  }
}
