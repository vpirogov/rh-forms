const MinLength = 4
const PwdLength = 8
const PwdRegExp =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/

export default function (obj) {
  // common checks
  const fields = ['name', 'username', 'password1', 'password2']

  for (let i = 0; i < fields.length; i++) {
    const f = fields[i]

    if (obj.hasOwnProperty(f)) {
      if (typeof obj[f] !== 'string') return { error: `${f} is not string` }
    } else {
      return { error: `No ${f} property` }
    }
  }

  // special checks
  // username:
  if (!obj.username.match(/^([a-zA-Z0-9_]|\.|-)+$/)) {
    return { error: 'username must contain only [a-zA-Z0-9_-.] symbols' }
  }
  if (obj.username.length < MinLength) {
    return { error: `username length is less then ${MinLength}` }
  }

  // name:
  if (obj.name.length < MinLength) {
    return { error: `name length is less then ${MinLength}` }
  }

  // password:
  if (obj.password1 !== obj.password2) {
    return { error: 'passwords mismatch' }
  }
  if (obj.password1.length < PwdLength) {
    return { error: `password length is less then ${PwdLength}` }
  }
  if (!obj.password1.match(PwdRegExp)) {
    return {
      error: `password must contain 1 lowercase, 1 uppercase, 1 numeric and 1 special character`,
    }
  }

  return obj
}
