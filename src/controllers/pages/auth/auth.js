import FormReader from '../../../components/formReader/formReader.js'
import Crypt from '../../../components/crypt/crypt.js'
import validator from './validator.js'

import signInTmpl from './views/signIn.hbs'
import signUpTmpl from './views/signUp.hbs'

const ExpirationTimeout = 4 * 3600 * 1000 // 4 hours in milliseconds
const DefaultRoles = ['member']

export default class Auth {
  constructor(ctx) {
    this.ctx = ctx
    this.resolve = null
  }

  run() {
    // returns promise
    // already authenticated
    const user = this.getSession()
    if (user) {
      this.ctx.menu.run()
      return new Promise((resolve) => resolve(user))
    }

    // not authenticated or expired
    this.ctx.target.innerHTML = signInTmpl()
    this.startListeners()

    return new Promise((resolve) => {
      this.resolve = resolve
    })
  }

  handleEvent(e) {
    switch (e.type) {
      case 'signIn':
        this.signIn()
        break
      case 'signUp':
        this.signUp()
        break
      case 'wannaSignIn':
        this.ctx.target.innerHTML = signInTmpl()
        break
      case 'wannaSignUp':
        this.ctx.target.innerHTML = signUpTmpl()
        break
    }
  }

  signIn() {
    const creds = {}

    const reader = new FormReader('signin', false)

    reader.read().fields.forEach((f) => {
      creds[f.key] = f.new
    })
    creds.password = Crypt.hash(creds.password)

    this.ctx.db.authUser(creds).then((auth) => {
      if (auth.length) {
        this.stopListeners()

        const user = auth[0]
        this.setLastSeen(user)
        this.saveSession(user)

        this.ctx.menu.run()
        this.resolve(user)
      } else this.ctx.alerter.warn('Authentication failed')
    })
  }

  setLastSeen(user) {
    this.ctx.db.patch({
      coll: 'app-users',
      id: user._id.$oid,
      key: 'lastSeen',
      value: Date.now(),
    })
  }

  signUp() {
    const reader = new FormReader('signup', false)

    const data = {}
    reader.read().fields.forEach((item) => (data[item.key] = item.new))

    const validation = validator(data)
    if (validation.error) {
      return this.ctx.alerter.warn(validation.error)
    }

    this.checkUsername(data.username)
      .then(() => signUpComposer(validation))
      .catch((error) => this.ctx.alerter.error(error.message))
      .then((userObj) => {
        if (userObj) {
          return this.ctx.db.createUser(userObj)
        }
      })
      .then((res) => {
        if (res && res.statusText === 'Created') {
          this.ctx.alerter.success('Successfully registered')
          this.ctx.target.innerHTML = signInTmpl()
        }
      })
  }

  checkUsername(username) {
    return this.ctx.db.getUserByUsername(username).then((res) => {
      if (res.length && res[0].username === username) {
        throw new Error('user exists')
      }
    })
  }

  saveSession(user) {
    user.expire = Date.now() + ExpirationTimeout
    localStorage.setItem('session', this.ctx.serializer.pack(user))
  }

  getSession() {
    let user = localStorage.getItem('session')

    if (user && typeof user === 'string') {
      user = this.ctx.serializer.unpack(user)
    }

    if (user && user.hasOwnProperty('expire')) {
      if (user.expire > Date.now()) {
        this.saveSession(user) // update expiration
        return user
      } else {
        localStorage.removeItem('session')
      }
    }

    return null
  }

  startListeners() {
    this.ctx.target.addEventListener('signIn', this)
    this.ctx.target.addEventListener('signUp', this)
    this.ctx.target.addEventListener('wannaSignIn', this)
    this.ctx.target.addEventListener('wannaSignUp', this)
  }

  stopListeners() {
    this.ctx.target.removeEventListener('signIn', this)
    this.ctx.target.removeEventListener('signUp', this)
    this.ctx.target.removeEventListener('wannaSignIn', this)
    this.ctx.target.removeEventListener('wannaSignUp', this)
  }

  logout() {
    localStorage.removeItem('session')
  }
}

const signUpComposer = (data) => {
  data.password = Crypt.hash(data.password1)
  delete data.password1
  delete data.password2

  data.roles = DefaultRoles
  data.userid = Crypt.userid(data)
  return data
}
