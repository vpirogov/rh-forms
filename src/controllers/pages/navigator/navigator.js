import ShowTable from '../table/showTable.js'

import navigatorTmpl from './views/navigator.hbs'
import aboutTmpl from './views/about.hbs'
import schemasListTmpl from './views/schemasList.hbs'

export default class Navigator {
  constructor(ctx) {
    this.ctx = ctx
    this.target = null
  }

  run() {
    // this.ctx.target.innerHTML = navigatorTmpl()
    this.roles = this.ctx.auth.getSession().roles

    this.ctx.db.getSections(this.roles).then((res) => {
      const cached = sessionStorage.getItem('section')

      const sects = res.map((r) => {
        return {
          name: r,
          visibility: r === cached ? 'visible' : 'hidden',
        }
      })

      let hasCached = false
      res.forEach((r) => {
        if (r === cached) hasCached = true
      })

      this.ctx.target.innerHTML = navigatorTmpl({
        sects,
        infoVisibility: hasCached ? 'hidden' : 'visible',
      })

      this.target = document.querySelector('div#schematarget')

      if (hasCached) {
        this.section = cached
        this.showSchemaList()
      } else {
        this.target.innerHTML = aboutTmpl()
      }

      this.ctx.target.firstChild.addEventListener('click', this)
    })
  }

  handleEvent(e) {
    // console.dir("it happens", e.target)
    // only buttons
    const el = e.target.closest('button') || null
    if (!el) return

    // get section and compare to current section
    const section = el.querySelector('span').textContent
    if (section === this.section) return
    this.section = section
    sessionStorage.setItem('section', section)

    // new section, so change mark
    Array.from(this.ctx.target.querySelectorAll('i')).forEach(
      (i) => (i.style.visibility = 'hidden')
    )
    el.querySelector('i').style.visibility = 'visible'

    // go to new section
    if (section === ' information') {
      // not really section
      this.ctx.router.extra({})
      this.target.innerHTML = aboutTmpl()
    } else {
      this.showSchemaList()
    }
  }

  showSchemaList() {
    this.ctx.db.getSchemas(this.section, this.roles).then((schemas) => {
      this.target.innerHTML = schemasListTmpl(schemas)
      this.ctx.router.extra(this.makeRoutes(schemas))
    })
  }

  makeRoutes(schemas) {
    const extra = {}

    schemas.map((s) => {
      let key = '#schema_' + s._id.$oid
      extra[key] = new ShowTable(s, this.ctx)
    })

    return extra
  }
}
