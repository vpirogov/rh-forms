import menuTmpl from './views/menu.hbs'

export default class Menu {
  constructor(selector, ctx) {
    this.target = document.querySelector(selector)
    this.ctx = ctx
  }

  run() {
    this.target.innerHTML = menuTmpl({
      name: this.ctx.auth.getSession().name,
    })

    this.target.addEventListener('click', this)
  }

  stop() {
    // turn off menu
    this.target.removeEventListener('click', this)
    this.target.innerHTML = ''
    // logout & reload page
    this.ctx.auth.logout()
    this.ctx.router.reload()
  }

  handleEvent(e) {
    const el = e.target.closest('button')
    if (!el) return

    if (el.dataset.action === 'logout') {
      this.stop()
    }
  }
}
