import Composer from './composer.js'

const c = new Composer()

const obj = {
  name: 'Agirish',
  type: 'pgt',
  population: 222,
  channel: {
    type: 'optics',
    bw: 220,
  },
  qwe: {
    ewq: {
      0: 42,
    },
  },
  first: { second: { third: { forth: 12 } } },
}

test('None-existent simple key', () => {
  expect(c.getVal('names', obj)).toBe(null)
})

test('Simple key', () => {
  expect(c.getVal('name', obj)).toBe('Agirish')
  expect(c.getVal('type', obj)).toBe('pgt')
  expect(c.getVal('population', obj)).toBe(222)
})

test('None-existent nested key', () => {
  expect(c.getVal('a.b.c', obj)).toBe(null)
  expect(c.getVal('first.second.b', obj)).toBe(null)
  expect(c.getVal('first.second.b.c', obj)).toBe(null)
  expect(c.getVal('first.second.b.c.d', obj)).toBe(null)
  expect(c.getVal('qwe.ewq.0.1', obj)).toBe(null)
})

test('Nested keys', () => {
  expect(c.getVal('channel.type', obj)).toBe('optics')
  expect(c.getVal('channel.bw', obj)).toBe(220)
  expect(c.getVal('qwe.ewq.0', obj)).toBe(42)
  expect(c.getVal('first.second.third.forth', obj)).toBe(12)
})
