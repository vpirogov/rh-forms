export default class Composer {
  getVal(key, obj) {
    let keys = key.split('.')
    let o = obj
    while (typeof o === 'object') {
      if (!keys.length) return null
      const k = keys.shift()
      if (k in o) {
        o = o[k]
      } else {
        return null
      }
    }
    if (keys.length) {
      return null
    }
    return o !== 'undefined' ? o : null
  }
  join(schema, data) {
    const arr = []
    data.forEach((d) => {
      const row = []
      schema.forEach((s) => {
        let cell = {
          key: s.key,
          type: s.type,
          write: s.write,
          id: d._id.$oid,
          value: this.getVal(s.key, d),
        }
        cell.isNull = cell.value === null
        row.push(cell)
      })
      arr.push(row)
    })
    return arr
  }
}
