export default class Modal {
  constructor() {
    this.modal = document.querySelector('div#modal')
    this.target = document.querySelector('div#modal-content')
  }

  show(content) {
    this.target.innerHTML = content
    this.modal.style.display = 'block'
  }

  hide() {
    this.target.innerHTML = ''
    this.modal.style.display = 'none'
  }

  onclick(handler) {
    this.target.firstChild.addEventListener('click', handler)
  }
}
