export default {
  keysFromArray(arr) {
    const keys = {}
    arr.forEach((obj) => {
      getKeys(keys, '', obj)
    })
    let keysArr = []
    for (let k of Object.keys(keys)) {
      keysArr.push({
        key: k,
        type: keys[k].sort().join(' '),
      })
    }
    return keysArr.sort((a, b) => a.key.localeCompare(b.key))
  },
}

const getKeys = (keys, prefix, obj) => {
  for (let [k, v] of Object.entries(obj)) {
    if (['_id', '_etag'].includes(k)) continue

    switch (typeof v) {
      case 'string':
      case 'number':
      case 'boolean':
      case 'undefined':
      case 'symbol':
        setKey(k, typeof v, prefix, keys)
        break
      case 'object':
        if (Array.isArray(v)) {
          setKey(k, 'array', prefix, keys)
        } else if (v === null) {
          setKey(k, 'null', prefix, keys)
        } else {
          let pref = k
          if (prefix) pref = prefix + '.' + k
          getKeys(keys, pref, obj[k])
        }
        break
      default:
        console.log('unknown type:', k, typeof v)
    }
  }
}

const setKey = (key, val, prefix, obj) => {
  let k = key
  if (prefix) k = prefix + '.' + key
  if (Array.isArray(obj[k])) {
    if (!obj[k].includes(val)) {
      obj[k].push(val)
    }
  } else {
    obj[k] = [val]
  }
}
