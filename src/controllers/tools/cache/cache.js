const ItemName = 'appcache'

export default class Cache {
  constructor(ctx) {
    this.ctx = ctx
    this.cache = this._load() || {}
  }

  get(key) {
    const path = this._getPath()

    if (
      this.cache.hasOwnProperty(path) &&
      this.cache[path].hasOwnProperty(key)
    ) {
      return this.cache[path][key]
    }

    return null
  }

  put(key, value) {
    const path = this._getPath()

    if (!this.cache.hasOwnProperty(path)) {
      this.cache[path] = {}
    }
    this.cache[path][key] = value

    this._save()
  }

  push(key, value) {
    const path = this._getPath()

    if (!this.cache.hasOwnProperty(path)) {
      this.cache[path] = {}
    }
    if (!this.cache[path].hasOwnProperty(key)) {
      this.cache[path][key] = []
    }
    this.cache[path][key].push(value)

    this._save()
  }

  delete(key) {
    const path = this._getPath()

    if (
      this.cache.hasOwnProperty(path) &&
      this.cache[path].hasOwnProperty(key)
    ) {
      delete this.cache[path][key]
    }

    this._save()
  }

  _getPath() {
    return this.ctx.schema._id.$oid
  }

  _save() {
    const data = this.ctx.serializer.pack(this.cache)
    localStorage.setItem(ItemName, data)
  }

  _load() {
    let data = localStorage.getItem(ItemName)

    if (data) {
      try {
        data = this.ctx.serializer.unpack(data)
      } catch {
        return null
      }
      return data
    }

    return null
  }
}
