export default function (data) {
  return data.reduce((str, row) => {
    const fields = row.map((field) => {
      // change '"' with '""'
      field.replace(/"/g, '""')
      // remove CR (excel hack)
      field.replace(/\r\n/g, '')

      return field
    })
    // wrap with '"' and join to string
    return (str += `"${fields.join('";"')}"\r\n`)
  }, '')
}
