import makeCSV from './makeCSV.js'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts'
pdfMake.vfs = pdfFonts.pdfMake.vfs

const TableSelector = 'table.m-table'

export default function (format, ctx) {
  const caption = document.querySelector('span#caption').textContent
  const description = document.querySelector('span#description').textContent

  const fileName = makeFilename(caption, format)
  const data = readTable()

  switch (format) {
    case 'csv':
      const CSVText = makeCSV(data)
      const BOM = new Uint8Array([0xef, 0xbb, 0xbf])
      const blob = new Blob([BOM, CSVText], { type: 'text/csv;charset=utf-8' })
      download(blob, fileName)
      break
    case 'pdf':
      const dd = makeDocumentDefinition(caption, description, data, ctx)
      pdfMake.createPdf(dd).download(fileName)
  }
}

const makeDocumentDefinition = (caption, description, data, ctx) => {
  data[0] = data[0].map((cell) => {
    return { text: cell.toUpperCase(), style: 'tableHeader' }
  })
  return {
    pageSize: 'A4',
    pageOrientation: 'landscape',
    watermark: {
      text: 'БТИ ХМФ',
      color: 'blue',
      opacity: 0.03,
      bold: false,
      italics: false,
    },
    footer: function (currentPage, pageCount) {
      return {
        style: 'footer',
        layout: 'noBorders',
        table: {
          widths: ['*', '*', '*'],
          body: [
            [
              `стр. ${currentPage.toString()} из ${pageCount}`,
              ctx.auth.getSession().name + '@lomonosov.bti86.ur.rt.ru',
              getDate(),
            ],
          ],
        },
      }
    },
    content: [
      { text: caption, style: 'header' },
      { text: description, style: 'subheader' },
      {
        style: 'tableExample',
        layout: 'lightHorizontalLines',
        table: {
          widths: data[0].map((cell, idx) => {
            if (idx) return '*'
            else return 'auto'
          }),
          headerRows: 1,
          body: data,
        },
      },
    ],
    styles: {
      header: {
        fontSize: ctx.pdfFontSize + 4,
        bold: true,
        margin: [0, 0, 0, 5],
      },
      subheader: {
        fontSize: ctx.pdfFontSize,
        italics: true,
        opacity: 0.6,
      },
      tableExample: {
        margin: [0, 5, 0, 15],
      },
      tableHeader: {
        bold: true,
        fontSize: ctx.pdfFontSize + 2,
        color: '#3388bb',
      },
      footer: {
        margin: [40, 10, 0, 0],
        opacity: 0.4,
        fontSize: 8,
      },
    },
    defaultStyle: {
      // alignment: 'justify'
      fontSize: ctx.pdfFontSize,
    },
  }
}

const download = (blob, fileName) => {
  const a = document.createElement('A')
  a.setAttribute('download', fileName)
  a.href = URL.createObjectURL(blob)

  a.click()
  URL.revokeObjectURL(a.href)
  a.remove()
}

// const makeCSV = data => data.reduce((str, row) => {
//     const fields = row.map(field => {
//         // change '"' with '""'
//         field.replace(/"/g, '""')
//         // remove CR (excel hack)
//         field.replace(/\r\n/g, "\n")

//         return field
//     })
//     // wrap with '"' and join to string
//     return str += `"${fields.join('","')}"\r\n`
// }, "")

const readTable = () => {
  const rows = Array.from(document.querySelector(TableSelector).rows)
  return rows
    .filter((r) => r.style.display !== 'none')
    .map((r) => {
      return Array.from(r.cells)
        .filter((c) => c.style.display !== 'none')
        .map((c) => c.textContent.trim())
    })
}

const makeFilename = (caption, format) => {
  const now = new Date()
  const time = [
    now.getFullYear(),
    now.getMonth() + 1,
    now.getDate(),
    '-',
    now.getHours(),
    now.getMinutes(),
  ]
    .map((item) => {
      if (typeof item === 'number' && item < 9) {
        return '0' + item
      }
      return item
    })
    .join('')

  return `${caption} ${time}.${format}`
}

const getDate = () => {
  const now = new Date()
  return (
    [
      now.getDate(),
      '.',
      now.getMonth() + 1,
      '.',
      now.getFullYear(),
      ' ',
      now.getHours(),
      ':',
      now.getMinutes(),
    ]
      .map((item) => {
        if (typeof item === 'number' && item < 9) {
          return '0' + item
        }
        return item
      })
      .join('') + ` UTC${now.getTimezoneOffset() / 60}`
  )
}
