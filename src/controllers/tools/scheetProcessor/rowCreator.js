export default {
  run(ctx) {
    const { key, type } = readPage()
    let value = ''
    if (type === 'integer' || type === 'float') value = 0

    const doc = {}
    doc[key] = value

    return ctx.db.post(ctx.schema.query.coll)
  },
}

const readPage = () => {
  const el = document.querySelector('table.m-table').tBodies[1].rows[0].cells[1]
  return {
    key: el.dataset.key,
    type: el.dataset.type,
  }
}
