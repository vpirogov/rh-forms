import Modal from '../../tools/modal/modal.js'

import statsTmpl from './views/stats.hbs'

const tableSelector = 'table.m-table'

export default function stats(col, type) {
  const vals = getVals(col, type)
  const obj = {}
  vals.forEach((v) => {
    if (obj[v]) {
      obj[v]++
    } else {
      obj[v] = 1
    }
  })
  const res = Object.keys(obj)
    .map((k) => {
      return { val: `"${k}"`, qty: obj[k] }
    })
    .sort(sortFn)

  const alias =
    document.querySelector(tableSelector).tBodies[0].rows[0].cells[col]
      .textContent

  const modal = new Modal()
  modal.show(statsTmpl({ res, alias }))
  modal.target.style.maxWidth = 300 + 'px'
  modal.onclick((e) => {
    if (e.target.matches('button.cancel')) {
      modal.hide()
      modal.target.style.maxWidth = ''
    }
  })
}

const sortFn = (b, a) => a.qty - b.qty

const getVals = (col, type) => {
  const rows = Array.from(document.querySelector(tableSelector).tBodies[1].rows)

  return rows
    .filter((r) => r.style.display === '')
    .map((r) => {
      let val = r.cells[col].innerHTML
      if (type === 'float') {
        val = parseFloat(val)
      } else if (type === 'integer') {
        val = parseInt(val, 10)
      }
      if (!val) {
        val = 'null'
      }
      return val
    })
}
