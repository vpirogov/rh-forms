import marker from './marker.js'
import sorter from './sorter.js'
import math from './math.js'
import stats from './stats.js'
import columnHider from './columnHider.js'

import cellCMTmpl from './views/contextMenuCell.hbs'
import columnCMTmpl from './views/contextMenuCol.hbs'

const tableSelector = 'table.m-table'

export default {
  cell(e, ctx) {
    const row = e.target.closest('TR').rowIndex - 1 // table header correction
    const origRow = parseInt(e.target.closest('TR').dataset.index, 10)
    const cell = e.target.cellIndex

    const cm = showCellCM()
    adjustPosition(cm, e)

    cm.addEventListener('markCell', (e) => {
      marker.markOne({
        ctx,
        row,
        origRow,
        cell,
        bg: e.detail.bg,
      })
      cm.remove()
    })
  },
  column(e, ctx) {
    const th = e.target.closest('th')

    if (th.cellIndex === 0) return

    const col = th.cellIndex
    const type = th.dataset.type

    let isNumber = false
    if (type === 'integer' || type === 'float') isNumber = true

    const cm = showColumnCM({ col, isNumber })
    adjustPosition(cm, e)

    cm.addEventListener('sort', (e) => {
      const obj = e.detail
      obj.type = type
      sorter.run(obj, ctx, false)

      cm.remove()
    })
    cm.addEventListener('math', (e) => {
      math(col, type)
      cm.remove()
    })
    cm.addEventListener('stats', (e) => {
      stats(col, type)
      cm.remove()
    })
  },
}

const showColumnCM = (col) => {
  const div = document.createElement('DIV')
  div.innerHTML = columnCMTmpl(col)
  div.style.position = 'absolute'
  document.body.append(div)
  return div
}

const showCellCM = (row, cell) => {
  const div = document.createElement('DIV')
  div.innerHTML = cellCMTmpl({ row, cell })
  div.style.position = 'absolute'
  document.body.append(div)
  return div
}

const adjustPosition = (cm, e) => {
  const box = cm.getBoundingClientRect()

  let left = e.pageX - 3
  let top = e.pageY - 3

  if (document.documentElement.clientWidth - left - box.width < box.width / 2) {
    left -= box.width - 6
  }

  if (
    document.documentElement.clientHeight - top - box.height <
    box.height / 2
  ) {
    top -= box.height - 6
  }

  cm.style.left = left + 'px'
  cm.style.top = top + 'px'
}
