import FieldEditor from './fieldEditor.js'
import contextMenu from './contextMenu.js'
import marker from './marker.js'
import sorter from './sorter.js'
import filter from './filter.js'
import rowCreator from './rowCreator.js'
import columnHider from './columnHider.js'
import exporter from '../../tools/exporter/exporter.js'
import outdateIndicator from './outdateIndicator.js'
import numerator from './numerator.js'

export default class ScheetProcessor {
  constructor(ctx) {
    this.ctx = ctx
    this.editor = new FieldEditor(ctx)
  }

  run() {
    marker.markAll(this.ctx).then(() => {
      sorter.init(this.ctx)
      filter.init(this.ctx)
      columnHider.load(this.ctx)
      outdateIndicator(this.ctx)
    })

    const GCFriendlyEl = this.ctx.tableTarget.firstChild
    GCFriendlyEl.addEventListener('dblclick', this)
    GCFriendlyEl.addEventListener('contextmenu', this)
    GCFriendlyEl.addEventListener('click', this)
    GCFriendlyEl.addEventListener('filtertable', this)
    GCFriendlyEl.addEventListener('clearfilter', this)
    GCFriendlyEl.querySelector('its-a-filter').addEventListener(
      'itsafilter-change',
      () => numerator()
    )
  }

  handleEvent(event) {
    const el = event.target
    switch (event.type) {
      case 'dblclick':
        if (el.tagName === 'TD' && el.dataset.write === 'true') {
          this.editor
            .run(event.target)
            .then(() => this.ctx.router.reload())
            .catch((e) => console.error(e))
        }
        if (el.tagName === 'TH') {
          columnHider.hideColumn(el.cellIndex, this.ctx)
        }
        break
      case 'contextmenu':
        const cell = el.tagName === 'I' ? el.closest('TH') : el
        switch (cell.tagName) {
          case 'TD':
            event.preventDefault()
            return contextMenu.cell(event, this.ctx)
          case 'TH':
            event.preventDefault()
            return contextMenu.column(event, this.ctx)
        }
        break
      case 'filtertable':
        filter.run(this.ctx, false)
        break
      case 'clearfilter':
        filter.clear(this.ctx)
        break
      case 'click':
        const btn = el.closest('button')
        if (!btn) return

        switch (btn.dataset.action) {
          case 'back':
            if (
              this.ctx.hasOwnProperty('outdateIndicator') &&
              this.ctx.outdateIndicator
            ) {
              clearTimeout(this.ctx.outdateIndicator)
            }
            return (document.location.href = '/#')
          case 'reload':
            this.ctx.router.reload()
            break
          case 'incPage':
            this.ctx.pager.incPage()
            this.ctx.router.reload()
            break
          case 'decPage':
            this.ctx.pager.decPage()
            this.ctx.router.reload()
            break
          case 'setPage':
            this.ctx.pager.setPage(btn.innerHTML)
            this.ctx.router.reload()
            break
          case 'incPageSize':
            this.ctx.pager.incPageSize()
            this.ctx.router.reload()
            break
          case 'decPageSize':
            this.ctx.pager.decPageSize()
            this.ctx.router.reload()
            break
          case 'setPageSize':
            this.ctx.pager.setPageSize(btn.innerHTML)
            this.ctx.router.reload()
            break
          case 'clearmarks':
            marker.clearAll(this.ctx)
            break
          case 'clearsorting':
            sorter.clearAll(this.ctx)
            this.ctx.router.reload()
            break
          case 'addrow':
            rowCreator.run(this.ctx).then(() => this.ctx.router.reload())
            break
          case 'exportcsv':
            exporter('csv')
            break
          case 'exportpdf':
            exporter('pdf', this.ctx)
            break
        }
    }
  }
}
