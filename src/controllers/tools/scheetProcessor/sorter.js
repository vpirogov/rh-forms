import numerator from './numerator.js'

const tableSelector = 'table.m-table'

export default {
  init(ctx) {
    const cache = ctx.cache.get('sort')

    if (cache) {
      this.run(cache, ctx, true)
    } else {
      numerator()
    }
  },

  run(obj, ctx, doNotCache) {
    if (!doNotCache) {
      ctx.cache.put('sort', obj)
    }
    sort(obj)
  },

  clearAll(ctx) {
    ctx.cache.delete('sort')
    Array.from(document.querySelectorAll('i.dirmark')).forEach((m) =>
      m.remove()
    )
  },
}

const sort = (obj) => {
  //
  // just renumber table if there is no input data
  if (!obj || !obj.hasOwnProperty('col')) return numerator()

  // got data
  const { col, direction, type } = obj
  const table = document.querySelector(tableSelector)
  const rows = Array.from(table.tBodies[1].rows)

  const arr = rows.map((row) => {
    const val = row.cells[col].innerHTML
    return {
      row,
      val: transformValue(val, type),
    }
  })
  arr.sort(getSortFn(type, direction))

  switchRows(arr, table)

  setSortedDirMark(col, direction)
  numerator()
}

const getSortFn = (type, direction) => {
  if (type === 'integer' || type === 'float') {
    return direction === 'asc' ? numAsc : numDesc
  } else {
    return direction === 'asc' ? strAsc : strDesc
  }
}

const numAsc = (a, b) => a.val - b.val
const numDesc = (b, a) => a.val - b.val
const strAsc = (a, b) => a.val.localeCompare(b.val)
const strDesc = (b, a) => a.val.localeCompare(b.val)

const switchRows = (arr, table) => {
  arr.forEach((a) => {
    table.tBodies[1].append(a.row)
  })
  for (let i = table.rows.length - 1; i > 0; i--) {
    if (!table.rows[i].innerHTML) {
      table.rows[i].remove()
    }
  }
}

const transformValue = (val, type) => {
  switch (type) {
    case 'integer':
      val = parseInt(val, 10)
      val = isNaN(val) ? Infinity : val
      break
    case 'float':
      val = parseFloat(val)
      val = isNaN(val) ? Infinity : val
      break
    default:
      val = val === '' ? 'яяяя' : val
  }
  return val
}

const setSortedDirMark = (col, dir) => {
  // remove previous marks
  Array.from(document.querySelectorAll('i.dirmark')).forEach((m) => m.remove())
  // create new mark
  const mark = document.createElement('I')
  mark.className =
    dir === 'asc' ? 'icon-sort-amount-asc' : 'icon-sort-amount-desc'
  mark.classList.add('dirmark', 'c-lime')
  // add to column header
  const th = document.querySelector(tableSelector).tBodies[0].rows[0].cells[col]
  th.prepend(mark)
}
