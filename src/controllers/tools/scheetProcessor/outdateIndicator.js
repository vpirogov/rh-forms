export default function (ctx) {
  // remove old indicator
  if (ctx.hasOwnProperty('outdateIndicator') && ctx.outdateIndicator) {
    clearTimeout(ctx.outdateIndicator)
  }
  // set up new indicator
  const btn = document.querySelector('button[data-action=reload]')
  if (btn) {
    ctx.outdateIndicator = setTimeout(() => {
      btn.style.color = 'orange'
    }, 120000)
  }
}
