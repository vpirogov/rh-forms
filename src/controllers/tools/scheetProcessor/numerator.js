const tableSelector = 'table.m-table'

export default function () {
  const rows = document.querySelector(tableSelector).tBodies[1].rows
  let num = 1
  Array.from(rows).forEach((r) => {
    if (r.style.display !== 'none') {
      r.cells[0].innerHTML = num++
    }
  })
}
