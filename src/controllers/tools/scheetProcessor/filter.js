import numerator from './numerator.js'

const FilterElSelector = 'input#filter'
const ScopeElSelector = 'select#filterscope'
const HLightClass = 'c-bg-gold'

export default {
  init(ctx) {
    // when table loads
    const cache = ctx.cache.get('filter')

    if (cache) {
      document.querySelector(FilterElSelector).value = cache.filter
      document.querySelector(ScopeElSelector).value = cache.scope
      this.run(ctx, true)
    }
  },
  run(ctx, doNotCache) {
    // when filter changes
    // read filter and scope from HTML
    const { filter, filterRE, scopeText, rows } = readPage()

    // caching
    if (!doNotCache) {
      ctx.cache.put('filter', {
        filter: filter.value,
        scope: scopeText,
      })
    }

    // hightlight filter input
    highLight(filter)

    // do filtering
    if (scopeText === 'any') {
      filterAny(filterRE, rows)
    } else {
      const col = findCol(scopeText)
      filterCol(filterRE, rows, col)
    }

    numerator()
  },
  clear(ctx) {
    const { filter, scope } = readPage()

    filter.value = ''
    scope.value = 'any'

    this.run(ctx, false)
  },
}

const highLight = (filter) => {
  if (filter.value === '') {
    filter.classList.remove(HLightClass)
  } else {
    filter.classList.add(HLightClass)
  }
}

const filterCol = (filterRE, rows, col) => {
  rows.forEach((r) => {
    if (filterRE.exec(r.cells[col].textContent)) {
      r.style.display = ''
    } else {
      r.style.display = 'none'
    }
  })
}

const findCol = (text) => {
  let col = null
  Array.from(document.querySelectorAll('th')).forEach((th) => {
    if (th.textContent.trim().toUpperCase() === text) col = th.cellIndex
  })
  return col
}

const filterAny = (filterRE, rows) => {
  rows.forEach((r) => {
    let hide = true
    for (let i = 1; i < r.cells.length; i++) {
      if (filterRE.exec(r.cells[i].textContent)) {
        hide = false
        break
      }
    }

    if (hide) {
      r.style.display = 'none'
    } else {
      r.style.display = ''
    }
  })
}

const readPage = () => {
  const filter = document.querySelector(FilterElSelector)
  const scope = document.querySelector(ScopeElSelector)
  return {
    filter,
    scope,
    filterRE: new RegExp(filter.value, 'i'),
    scopeText: scope.value,
    rows: Array.from(document.querySelector('table.m-table').tBodies[1].rows),
  }
}
