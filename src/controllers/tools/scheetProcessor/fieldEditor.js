import Modal from '../../tools/modal/modal.js'
import DataTypes from '../../tools/datatypes/datatypes.js'

import fieldEditorTmpl from './views/fieldEditor.hbs'
import historyTmpl from './views/history.hbs'

export default class FieldEditor {
  constructor(ctx) {
    this.ctx = ctx
    this.modal = new Modal()
    this.data = {}
    this.resolve = null
    this.reject = null
  }

  run(cell) {
    if (cell.tagName !== 'TD') {
      setTimeout(() => this.reject(new Error("Cell is not 'TD'")), 0)
    } else {
      this.data = {
        key: cell.dataset['key'],
        value: this.ctx.serializer.unpack(cell.dataset['value']),
        type: cell.dataset['type'],
        id: cell.dataset['id'],
      }

      this.addDictionary(this.data).then(() => {
        this.modal.show(fieldEditorTmpl(this.data))
        this.modal.onclick(this)
        this.modal.target.querySelector('.w3-input').focus()
      })
    }

    return new Promise((resolve, reject) => {
      this.resolve = resolve
      this.reject = reject
    })
  }

  handleEvent(event) {
    const el = event.target
    if (el.tagName !== 'BUTTON') return
    if (el.classList.contains('cancel')) return this.finish()
    if (el.classList.contains('save')) return this.save()
    if (el.classList.contains('history'))
      return this.loadHistory(event.target.dataset)
  }

  save() {
    const el = this.modal.target.querySelector('.w3-input')
    this.data.new = el.value

    if (el.value !== this.data.value) {
      this.convertType()
        .then(() =>
          this.safePatch(
            Object.assign(this.data, {
              coll: this.ctx.schema.query.coll,
            })
          )
        )
        .catch((e) => this.notify(e.message))
        .finally(() => this.finish())
    } else {
      this.finish()
    }
  }

  loadHistory(dataset) {
    const id = `${this.ctx.schema.query.coll}-${dataset.id}-${dataset.key}`
    this.ctx.db.getOne('app-history', id).then((res) => {
      let data = []
      if (res.hasOwnProperty('history')) {
        data = res.history.sort((a, b) => b.when - a.when)
      }
      this.modal.target.querySelector('div#history').innerHTML = historyTmpl({
        history: data,
        type: dataset.type,
      })
    })
  }

  safePatch(obj) {
    return this.ctx.db.getVal(obj).then((res) => {
      if (res[obj.key] && res[obj.key] !== obj.value) {
        throw new Error('Field has been changed after loading')
      } else {
        obj.value = obj.new // db.patch waiting for {value}
        this.log(obj)
        return this.ctx.db.patch(obj)
      }
    })
  }

  log({ coll, id, key, value }) {
    const logId = `${coll}-${id}-${key}`
    this.ctx.db.patch({
      coll: 'app-history',
      id: logId,
      key: '$push',
      value: {
        history: {
          value,
          who: this.ctx.auth.getSession().name,
          when: Date.now(),
        },
      },
    })
  }

  convertType() {
    // ["string","integer","float", "boolean", "datestr", "text"]
    return new Promise((resolve, reject) => {
      switch (this.data.type) {
        case 'integer':
          this.data.new = parseInt(this.data.new)
          if (isNaN(this.data.new))
            reject(new Error(`Cannot parse ${this.data.type}`))
          else resolve()
          break
        case 'float':
          this.data.new = parseFloat(this.data.new)
          if (isNaN(this.data.new))
            reject(new Error(`Cannot parse ${this.data.type}`))
          else resolve()
          break
        case 'boolean':
          this.data.new = this.data.new === 'true'
        default:
          resolve()
      }
    })
  }

  addDictionary() {
    if (DataTypes.includes(this.data.type)) {
      return new Promise((resolve) => resolve())
    }

    return this.ctx.db
      .getDictionaryContentByName(this.data.type)
      .then((dict) => (this.data.dict = dict))
  }

  notify(text) {
    console.warn(text)
  }

  finish() {
    this.modal.hide()
    this.resolve()
  }
}
