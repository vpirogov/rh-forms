const Collection = 'app-marks'

// const cache = []

export default {
  markOne({ cell, row, origRow, bg, ctx }) {
    // Mark cell. Row is a number of tr in the table.
    mark({ cell, row, bg })

    // Save mark state
    origRow = this.addPagerOffset(origRow, ctx.pager)
    let key = `r${origRow}-c${cell}`
    // set by default
    let value = {
      row: origRow, // pager adjustment
      cell,
      bg,
    }
    // unset if empty
    if (bg === '') {
      value = {}
      value[key] = true
      key = '$unset'
    }
    // execute patch
    ctx.db.patch({
      coll: Collection,
      id: makeId(ctx),
      key,
      value,
    })
  },

  addPagerOffset(row, pager) {
    return row + (pager.getPage() - 1) * pager.getPageSize()
  },

  removePagerOffset(row, pager) {
    return row - (pager.getPage() - 1) * pager.getPageSize()
  },

  markAll(ctx) {
    return ctx.db.getOne(Collection, makeId(ctx)).then((marks) => {
      if (marks.hasOwnProperty('_id')) {
        Object.entries(marks).forEach((ent) => {
          if (ent[0] === '_id' || ent[0] === '_etag') return
          ent[1].row = this.removePagerOffset(ent[1].row, ctx.pager)
          mark(ent[1])
        })
      }
    })
  },

  clearAll(ctx) {
    ctx.db
      .getOne(Collection, makeId(ctx))
      .then((marks) => {
        if (marks.hasOwnProperty('_id')) {
          Object.entries(marks).forEach((ent) => {
            if (ent[0] === '_id' || ent[0] === '_etag') return
            const obj = ent[1]
            obj.bg = ''
            mark(obj)
          })
        }
      })
      .then(() => ctx.db.delete(Collection, makeId(ctx)))
  },
}

const mark = ({ cell, row, bg }) => {
  if (cell !== undefined && row !== undefined) {
    const tr = document.querySelector('table').tBodies[1].rows[row]
    if (tr) {
      const td = tr.cells[cell]
      if (td) {
        td.style.backgroundColor = bg
      }
    }
  }
}

const makeId = (ctx) => {
  const userid = ctx.auth.getSession().userid
  const schema = ctx.schema._id.$oid

  return `${userid}-${schema}`
}
