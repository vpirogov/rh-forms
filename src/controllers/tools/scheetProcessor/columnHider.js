import hiddenColsTmpl from './views/hiddenColumns.hbs'

export default {
  load(ctx) {
    const cached = ctx.cache.get('hidden')
    if (!cached) return

    showHiddenList(cached.sort(sortFn), (e) => {
      if (!e.target.matches('span.w3-tag')) return

      const alias = e.detail.alias
      ctx.cache.put(
        'hidden',
        cached.filter((c) => c !== alias)
      )
      ctx.router.reload()
    }) // TODO refactoring needed

    // hide
    const THs = document.querySelector('table.m-table').tBodies[0].rows[0].cells

    Array.from(THs)
      // .filter(th => {
      //     for(let i = 0; i < cached.length; i++) {
      //         if (cached[i] === th.textContent.trim().toUpperCase()) {
      //             return true
      //         }
      //     }
      //     return false
      // })
      .filter((th) =>
        cached.reduce(
          (res, c) => (res = res || c === th.textContent.trim().toUpperCase()),
          false
        )
      )
      .map((th) => th.cellIndex)
      .forEach((idx) => hide(idx))
  },

  hideColumn(col, ctx) {
    const colAlias = getColName(col)
    ctx.cache.push('hidden', colAlias)
    ctx.router.reload()
  },
}

const hide = (col) => {
  Array.from(document.querySelector('table.m-table').rows).forEach((r) => {
    r.cells[col].style.display = 'none'
  })
}

const getColName = (col) => {
  const th =
    document.querySelector('table.m-table').tBodies[0].rows[0].cells[col]
  return th.textContent.trim().toUpperCase()
}

const showHiddenList = (hidden, listener) => {
  const target = document.querySelector('div.hiddencols')
  target.innerHTML = hiddenColsTmpl(hidden)

  target.firstChild.addEventListener('showcolumn', listener)
}

const sortFn = (a, b) => a.localeCompare(b)
