import Modal from '../../tools/modal/modal.js'

import mathTmpl from './views/math.hbs'

const tableSelector = 'table.m-table'

export default function math(col, type) {
  const vals = getVals(col, type)
  const numVals = vals.filter((v) => !isNaN(v))

  const res = [
    { name: 'Total', val: vals.length },
    { name: 'Numeric', val: numVals.length },
    { name: 'Sum', val: numVals.reduce((sum, v) => (sum += v), 0) },
    { name: 'Max', val: Math.max(...numVals) },
    { name: 'Min', val: Math.min(...numVals) },
  ]
  res.push({ name: 'Avg', val: res[2].val / res[1].val })

  const alias =
    document.querySelector(tableSelector).tBodies[0].rows[0].cells[col]
      .textContent

  const modal = new Modal()
  modal.show(mathTmpl({ res, alias }))
  modal.target.style.maxWidth = 300 + 'px'
  modal.onclick((e) => {
    if (e.target.matches('button.cancel')) {
      modal.hide()
      modal.target.style.maxWidth = ''
    }
  })
}

const getVals = (col, type) => {
  const rows = Array.from(document.querySelector(tableSelector).tBodies[1].rows)

  return rows
    .filter((r) => r.style.display === '')
    .map((r) => {
      let val = r.cells[col].innerHTML
      if (type === 'float') {
        return parseFloat(val)
      } else if (type === 'integer') {
        return parseInt(val, 10)
      } else {
        return null
      }
    })
}
