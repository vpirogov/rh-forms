export default class GlobalListeners {
  constructor(ctx) {
    this.ctx = ctx
  }
  set() {
    // document.addEventListener("keyup", this.keyUp)
    document.addEventListener('keydown', this)
  }

  // keyUp(e) {
  handleEvent(e) {
    if (e.code === 'Escape') {
      try {
        const btns = document.querySelectorAll('button.cancel')
        if (btns.length) btns[btns.length - 1].click()
      } catch {}
      return
    }

    if (!isCommandMode()) return

    if (e.code === 'KeyF') {
      e.preventDefault()
      const filter = document.querySelector('input#filter')
      if (filter) {
        filter.focus()
        filter.select()
      }
    }
    if (!e.ctrlKey && e.code === 'KeyC') {
      const clearFilterBtn = document.querySelector('button#clearfilter')
      if (clearFilterBtn) clearFilterBtn.click()
    }
    if (e.code === 'KeyS') {
      const scopeSel = document.querySelector('select#filterscope')
      if (scopeSel) scopeSel.focus()
    }
    if (!e.altKey && e.code === 'NumpadAdd') {
      const table = document.querySelector('table.m-table')
      if (table && this.ctx.fontSize < 20) {
        table.style.fontSize = ++this.ctx.fontSize + 'px'
        this.ctx.alerter.info(`font size = ${this.ctx.fontSize}`, 4000)
      }
    }
    if (!e.altKey && e.code === 'NumpadSubtract') {
      const table = document.querySelector('table.m-table')
      if (table && this.ctx.fontSize > 6) {
        table.style.fontSize = --this.ctx.fontSize + 'px'
        this.ctx.alerter.info(`font size = ${this.ctx.fontSize}`, 4000)
      }
    }
    if (e.altKey && e.code === 'NumpadAdd') {
      if (this.ctx.hasOwnProperty('pdfFontSize')) {
        if (this.ctx.pdfFontSize < 20) {
          this.ctx.pdfFontSize++
        }
        this.ctx.alerter.info(`PDF font size = ${this.ctx.pdfFontSize}`, 4000)
      }
    }
    if (e.altKey && e.code === 'NumpadSubtract') {
      if (this.ctx.hasOwnProperty('pdfFontSize')) {
        if (this.ctx.pdfFontSize > 4) {
          this.ctx.pdfFontSize--
        }
        this.ctx.alerter.info(`PDF font size = ${this.ctx.pdfFontSize}`, 4000)
      }
    }
    if (e.code === 'KeyP') {
      const btn = document.querySelector("button[data-action='exportpdf']")
      if (btn) btn.click()
    }
    if (e.code === 'Comma') {
      const btn = document.querySelector("button[data-action='exportcsv']")
      if (btn) btn.click()
    }
    if (e.code === 'KeyR') {
      this.ctx.router.reload()
    }
    if (e.code === 'ArrowLeft' || e.code === 'ArrowRight') {
      const scrollWrapper = document.querySelector('div#scrollwrapper')
      if (scrollWrapper) scrollWrapper.focus()
    }
  }
}

const isCommandMode = () => {
  const filter = document.querySelector('input#filter')
  const itsaf = document.querySelector('its-a-filter input[type=text]')
  const modal = document.querySelector('div#modal')
  return (
    document.activeElement !== filter &&
    document.activeElement !== itsaf &&
    modal.style.display === 'none'
  )
}
