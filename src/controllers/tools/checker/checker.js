import nameCheck from './nameCheck.js.js'
import filterCheck from './filterCheck.js'
import sortCheck from './sortCheck.js'

export default class Checker {
  constructor(target, collection, db) {
    this.target = target
    this.coll = collection
    this.db = db
  }
  handleEvent(event) {
    if (event.target.tagName !== 'TEXTAREA') return
    const el = event.target
    const statusEl = this.target.querySelector(
      'span#' + el.dataset.key + 'status'
    )
    // console.dir(el.dataset.key, el.value, statusEl.textContent);
    this.check(el.dataset.key, el.value, statusEl)
  }
  check(key, value, statusEl) {
    let result

    switch (key) {
      case 'filter':
        filterCheck(value, this.db, this.coll, statusEl, this.target)
        break
      case 'sort':
        result = sortCheck(value, this.target, this.db)
    }
  }
}
