let StatusElement = null
const OKClassName = 'c-green'
const FailClassName = 'c-red'

export default function (text, db, coll, statusEl, target) {
  StatusElement = statusEl
  let obj
  try {
    obj = JSON.parse(text)
  } catch (err) {
    finish('Invalid JSON', FailClassName)
    return
  }
  // not an array
  if (Array.isArray(obj)) {
    finish('Must be an object', FailClassName)
    return
  }
  console.dir('UP')
  // check for fields
  let cf = checkFields(obj, target)
  if (cf) {
    finish(cf, FailClassName)
    return
  }

  db.checkFilter(coll, obj)
    .then((response) => parseResponse(response))
    .then((result) => parseResult(result))
}

const checkFields = (obj, target) => {
  return false
  const rows = target.querySelector('table.existentfields').tBodies[1].rows
  const fields = []
  for (let i = 0; i < rows.length; i++) {
    fields.push(rows[i].cells[0].innerHTML)
  }
  for (let k in obj) {
    if (!fields.includes(k)) {
      return '"' + k + '" not found'
    }
  }
  return false
}

const parseResponse = (response) => Promise.all(response.map((r) => r.json()))
const parseResult = (result) => {
  // invalid filter => have error message
  for (let r of result) {
    if (r.message) {
      finish(r.message, FailClassName)
      return
    }
  }
  // valid filter
  let qty = result.map((r) => r._size)
  const text = `Filtered ${qty[1]} of ${qty[0]}`
  if (qty[0] === qty[1] || qty[1] === 0) {
    finish(text, FailClassName)
    return
  }
  finish(text, OKClassName)
}

const finish = (text, className) => {
  StatusElement.textContent = text
  StatusElement.className = className
}
