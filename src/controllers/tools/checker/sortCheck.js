export default function (text, target) {
  let obj
  try {
    obj = JSON.parse(text)
  } catch (err) {
    return 'Invalid JSON'
  }
  return check(obj, target)
}

const check = (obj, target) => {
  if (Array.isArray(obj)) {
    return 'Must be an object'
  }

  const rows = target.querySelector('table.existentfields').tBodies[1].rows
  const fields = []
  for (let i = 0; i < rows.length; i++) {
    fields.push(rows[i].cells[0].innerHTML)
  }
  for (let k in obj) {
    if (typeof obj[k] !== 'number') {
      return 'Only number 1 and -1 must be used'
    }
    if (obj[k] !== -1 && obj[k] !== 1) {
      return 'invalid value "' + obj[k] + '" for "' + k + '"'
    }
    if (!fields.includes(k)) {
      return '"' + k + '" not found'
    }
  }
  return 'OK'
}
