const defaultPageSize = 1000
const pageSizes = [
  10, 15, 20, 25, 30, 35, 40, 50, 80, 100, 150, 200, 300, 400, 500, 800, 1000,
]

export default class Pager {
  constructor(ctx) {
    this.ctx = ctx
    this._state = {
      page: 1,
      pageSize: 20,
    }

    this._count = 0 // rows count
  }

  init() {
    const cached = this.ctx.cache.get('pager')
    if (cached) {
      this._state = cached
    }
  }

  setCount(num) {
    this._count = num
  }

  _save() {
    this.ctx.cache.put('pager', this._state)
  }

  // PAGE SIZE
  getPageSize() {
    return this._state.pageSize
  }
  setPageSize(str) {
    const ps = parseInt(str, 10)
    if (!ps.isNaN) {
      this._state.pageSize = ps
      this._state.page = 1
      this._save()
    }
  }
  decPageSize() {
    let idx = pageSizes.indexOf(this.getPageSize())
    if (idx < 0) {
      this.setPageSize(defaultPageSize)
    }
    if (idx === 0) return
    this.setPageSize(pageSizes[idx - 1])
  }
  incPageSize() {
    let idx = pageSizes.indexOf(this.getPageSize())
    if (idx < 0) {
      this.setPageSize(defaultPageSize)
    }
    if (idx < pageSizes.length - 1) this.setPageSize(pageSizes[idx + 1])
  }

  // PAGE
  getPage() {
    return this._state.page
  }
  setPage(str) {
    const page = parseInt(str, 10)
    if (!page.isNaN) {
      this._state.page = page
      this._save()
    }
  }
  incPage() {
    this._state.page++
    this._save()
  }
  decPage() {
    if (this._state.page) {
      this._state.page--
      this._save()
    }
  }

  getStatus() {
    return {
      count: this._count,
      page: this.getPage(),
      pageSize: this.getPageSize(),
      pages: this._getPages(),
      hasNextPage: this._hasNextPage(),
      hasPrevPage: this._hasPrevPage(),
      hasNextPageSize: this._hasNextPageSize(),
      hasPrevPageSize: this._hasPrevPageSize(),
      pageSizes: this._getPageSizes(),
    }
  }
  _hasPrevPage() {
    return this.getPage() > 1
  }
  _hasNextPage() {
    return this._count > this.getPage() * this.getPageSize()
  }
  _hasPrevPageSize() {
    return pageSizes.indexOf(this.getPageSize()) !== 0
  }
  _hasNextPageSize() {
    return (
      pageSizes.indexOf(this.getPageSize()) !== pageSizes.length - 1 &&
      this._count > this.getPageSize()
    )
  }
  _getPages() {
    const ps = this.getPageSize()
    let pages = []
    for (let i = 1; i <= Math.ceil(this._count / ps); i++) {
      pages.push({
        selected: i === this._page,
        num: i,
      })
    }
    return pages
  }
  _getPageSizes() {
    const ps = this.getPageSize()

    return pageSizes
      .filter((size, idx, arr) => {
        return idx === 0 || this._count > arr[idx - 1]
      })
      .map((size) => {
        return {
          selected: size === ps,
          num: size,
        }
      })
  }
}
