module.exports = function ts2str(ts) {
  if (typeof ts === 'number') {
    const date = new Date(ts)
    return format(date)
  } else {
    return ts
  }
}

const format = (date) => {
  return (
    [
      date.getDate(),
      '.',
      date.getMonth() + 1,
      '.',
      date.getFullYear(),
      ' ',
      date.getHours(),
      ':',
      date.getMinutes(),
    ]
      .map((item) => {
        if (typeof item === 'number' && item < 10) {
          return '0' + item
        } else {
          return item
        }
      })
      .join('') +
    ' UTC' +
    date.getTimezoneOffset() / 60
  )
}
