import fetchObj from '../components/fetch/fetchObj.js'
import Alerter from '../components/alerter/alerter.js'

const alerter = new Alerter()

const appSchemasRE = /^app-.*/

export default class Task {
  constructor(API) {
    this.url = API.url
    this.fo = new fetchObj(API)
  }
  get(query, { pageSize, page }) {
    let url0 = this.url + '/' + query.coll + '/_size?'
    let url = this.url + '/' + query.coll + '/?'
    if (query.filter) {
      url0 += `filter=${query.filter}`
      url += `filter=${query.filter}`
    }
    if (query.sort) {
      url += '&sort=' + JSON.stringify(query.sort)
    }
    url += '&pagesize=' + pageSize
    url += '&page=' + page
    return Promise.all([
      fetch(url0, this.fo), // size
      fetch(url, this.fo), // data
    ]).catch((error) => err(error.message))
  }
  getOne(coll, id) {
    const url = `${this.url}/${coll}/${id}`
    return fetch(url, this.fo)
      .then((result) => result.json())
      .catch((error) => err(error.message))
  }
  getVal({ coll, id, key }) {
    let url = this.url + '/' + coll + '/' + id
    url += '?keys=' + "{'" + key + "':1}"
    return fetch(url, this.fo)
      .then((result) => result.json())
      .catch((error) => err(error))
  }
  patch({ coll, id, key, value }) {
    const url = this.url + '/' + coll + '/' + id
    const patch = {}
    patch[key] = value
    const fo = this.fo.create('PATCH', patch)
    return fetch(url, fo).catch((error) => err(error))
  }
  post(coll, doc) {
    const url = `${this.url}/${coll}`
    const fo = this.fo.create('POST', doc)
    return fetch(url, fo).catch((error) => err(error))
  }
  // put(coll,id,doc) {
  //     const url = `${this.url}/${coll}/${id}`
  //     const fo = this.fo.create("PUT", doc)
  //     return fetch(url, fo)
  //         .catch(error => err(error))
  // }
  delete(coll, id) {
    if (!id) {
      return new Promise((res, rej) => rej(new Error('Empty id for delete')))
    }
    const url = `${this.url}/${coll}/${id}`
    const fo = this.fo.create('DELETE', {})
    return fetch(url, fo).catch((error) => err(error))
  }
  getSections(roles) {
    let url = this.url + '/app-schemas'
    url += '?keys={section:1}'
    url += '&sort={section:1}'
    url += `&filter={roles:{$in:` + JSON.stringify(roles) + '}}'

    return fetch(url, this.fo)
      .then((result) => result.json())
      .then((sects) => sects.map((s) => s.section))
      .then((sects) => [...new Set(sects)])
      .catch((error) => err(error))
  }
  getSchemas(section, roles) {
    let url = this.url + '/app-schemas'
    url += `?filter={section:"${section}",`
    url += `roles:{"$in":` + JSON.stringify(roles) + '}}'
    url += '&sort={name:1}'
    return fetch(url, this.fo)
      .then((result) => result.json())
      .catch((error) => err(error))
  }
  authUser({ username, password }) {
    let url = `${this.url}/app-users`
    url += `?filter={"username":"${username}","password":"${password}"}`
    url += '&keys={"password":0,"username":0,"_etag":0}'
    return fetch(url, this.fo)
      .then((result) => result.json())
      .catch((error) => err(error))
  }
  getUserByUsername(username) {
    let url = `${this.url}/app-users`
    url += `?filter={"username":"${username}"}`
    url += '&keys={"username":1}'
    return fetch(url, this.fo)
      .then((result) => result.json())
      .catch((error) => err(error.message))
  }
  createUser(userObj) {
    const url = `${this.url}/app-users`
    const fo = this.fo.create('POST', userObj)
    return fetch(url, fo).catch((error) => err(error))
  }
  getDictionaryContent({ collection, field, filter }) {
    let url = `${this.url}/${collection}`
    url += `?keys={"${field}":1}&sort=${field}`
    url += `&filter=${filter}`
    return fetch(url, this.fo)
      .then((result) => result.json())
      .then((result) => result.map((r) => r[field]))
      .catch((error) => err(error.message))
  }
  getDictionaryContentByName(name) {
    let url = `${this.url}/app-dictionaries`
    url += '?keys={"_etag":0,}'
    url += `&filter={"name":"${name}"}`
    return fetch(url, this.fo)
      .then((result) => result.json())
      .then((dict) => this.getDictionaryContent(dict[0]))
      .catch((error) => err(error.message))
  }
}

const err = (text) => {
  alerter.error('provider errror: ' + text)
}
