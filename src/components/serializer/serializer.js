export default {
  pack(obj) {
    return window.btoa(unescape(encodeURIComponent(JSON.stringify(obj))))
  },
  unpack(text) {
    return JSON.parse(decodeURIComponent(escape(window.atob(text))))
  },
}
