export default class Alerter {
  constructor() {
    this.target = createTarget()
  }
  msg(type = 'info', text, delay = 10000) {
    const m = createMessageElement(text)

    switch (type) {
      case 'info':
        m.style.backgroundColor = 'lightblue'
        break
      case 'warn':
        m.style.backgroundColor = 'lightsalmon'
        break
      case 'error':
        m.style.backgroundColor = 'crimson'
        m.style.color = 'floralwhite'
        break
      case 'success':
        m.style.backgroundColor = 'lightgreen'
    }

    this.target.append(m)
    setTimeout(() => m.remove(), delay)
  }

  info(text, delay) {
    this.msg('info', text, delay)
  }
  warn(text, delay) {
    this.msg('warn', text, delay)
  }
  error(text, delay) {
    this.msg('error', text, delay)
  }
  success(text, delay) {
    this.msg('success', text, delay)
  }
}

const createMessageElement = (text) => {
  const m = document.createElement('div')
  m.style.padding = '4px 10px'
  m.style.marginTop = '2px'
  m.style.borderRadius = '4px'
  m.style.opacity = 0.8
  m.style.color = 'navy'
  m.ondblclick = function () {
    this.remove()
  }
  m.innerHTML = text

  return m
}

const createTarget = () => {
  const outer = document.createElement('div')
  outer.style.position = 'fixed'
  outer.style.bottom = '10px'
  outer.style.width = '100%'
  document.body.append(outer)

  const inner = document.createElement('div')
  inner.style.maxWidth = '500px'
  inner.style.margin = 'auto'
  outer.append(inner)

  return inner
}
