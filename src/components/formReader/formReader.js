export default class FormReader {
  constructor(domain, edit = false) {
    this.selector = `[data-domain=${domain}]`
    // Creational forms doesn't need 'data-value="something..."'
    // but editional form does.
    this.edit = edit
    this.fields = []
  }

  read() {
    Array.from(document.querySelectorAll(this.selector)).forEach((el) => {
      this.validate(el)
      this.parse(el)
    })

    // console.dir("all:", this.fields)
    // console.dir("changed:", this.changedFields)
    return this
  }

  get changedFields() {
    return this.fields.filter((f) => f.old !== f.new)
  }

  parse(el) {
    const obj = {
      key: el.dataset.key,
      old: el.dataset.value,
      new: el.value,
    }
    if (el.matches('input[type=checkbox]')) {
      obj.old = el.dataset.value === 'true'
      obj.new = el.checked
    }

    this.fields.push(obj)
  }

  validate(el) {
    if (!el.dataset.key) {
      console.error("'data-key' is not set:")
      console.dir(el)
    }
    if (this.edit && el.dataset.value === undefined) {
      console.error("'data-value' is not set:")
      console.dir(el)
    }
  }
}
