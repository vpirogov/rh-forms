export default class fetchObj {
  constructor({ username, password }) {
    this.method = 'GET'
    this.body = null
    this.credentials = 'include'
    this.headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic ' + btoa(username + ':' + password),
    }
  }
  set(method, bodyObj) {
    // buggy approach. obsolete
    this.method = method
    this.body = JSON.stringify(bodyObj)
  }
  clear() {
    // obsolete
    this.method = 'GET'
    this.body = null
  }
  create(method, bodyObj) {
    const obj = {}

    for (let key in this) {
      obj[key] = this[key]
    }

    obj.method = method
    obj.body = JSON.stringify(bodyObj)

    return obj
  }
}
