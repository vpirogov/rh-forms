const bcrypt = require('bcryptjs')
const sha1 = require('sha1')

export default {
  hash(cleartext) {
    const salt = '$2a$14$H5kWqAKqQQoWuvRrsBIxUu' // #dirtyhacktodo
    return bcrypt.hashSync(cleartext, salt)
  },
  userid(credentials) {
    return sha1(JSON.stringify(credentials) + Date.now())
  },
}
